package com.army;

import com.army.dao.UserDetailRepository;
import com.army.dao.UserRepository;
import com.army.dao.test1.UserTest1Repository;
import com.army.dao.test2.UserTest2Repository;
import com.army.datebean.UserInfo;
import com.army.vo.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootJpaApplicationTests {

    @Resource
    private UserRepository userRepository;

    @Resource
    private UserDetailRepository userDetailRepository;

    @Resource
    private UserTest1Repository userTest1Repository;
    @Resource
    private UserTest2Repository userTest2Repository;

    @Test
    public void test() {
        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG);
        String formatedDate = dateFormat.format(date);
        userRepository.deleteAll();
        userRepository.save(new User("aa", "aa123456", "aa@126.com", "aa", formatedDate));
        userRepository.save(new User("bb", "bb123456", "bb@126.com", "bb", formatedDate));
        userRepository.save(new User("cc", "cc123456", "cc@126.com", "cc", formatedDate));

        Assert.assertEquals(9, userRepository.findAll().size());
        Assert.assertEquals("bb", userRepository.findByUserNameOrEmail("bb", "cc@126.com").getNickName());

        userRepository.delete(userRepository.findByUserName("aa1"));
    }

    @Test
    public void testPageQuery() {
        int page=1, size=10;
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = PageRequest.of(page, size, sort);
        userRepository.findAll(pageable);
        userRepository.findByNickName("testName", pageable);
    }

    @Test
    public void testUserInfo() {
        List<UserInfo> userInfos = userDetailRepository.findUserInfo("打球");
        for(UserInfo userInfo : userInfos) {
            System.out.println("address " + userInfo.getAddress());
        }
    }

    @Test
    public void testSave() throws Exception {
        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG);
        String formattedDate = dateFormat.format(date);

        userTest1Repository.save(new User("aa", "aa123456", "aa@163.com", "aa", formattedDate));
        userTest1Repository.save(new User("bb", "bb123456", "bb@163.com", "bb", formattedDate));
        userTest2Repository.save(new User("cc", "cc123456", "cc@163.com", "cc", formattedDate));
    }


}
