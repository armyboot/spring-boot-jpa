package com.army.datebean;

public interface UserInfo {
    String getUserName();
    String getEmail();
    String getAddress();
    String getHobby();
}
